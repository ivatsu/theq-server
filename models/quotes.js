const ObjectID = require("mongodb").ObjectID;
const db = require("./../config/db");

exports.allRus = function(callback) {
  db.get()
    .collection("quotes/rus")
    .find()
    .toArray(function(error, docs) {
      callback(error, docs);
    });
};

exports.allEng = function(callback) {
  db.get()
    .collection("quotes/eng")
    .find()
    .toArray(function(error, docs) {
      callback(error, docs);
    });
};

exports.findRusById = function(id, callback) {
  db.get()
    .collection("quotes/rus")
    .findOne({ _id: ObjectID(id) }, function(error, doc) {
      callback(error, doc);
    });
};

exports.findEngById = function(id, callback) {
  db.get()
    .collection("quotes/eng")
    .findOne({ _id: ObjectID(id) }, function(error, doc) {
      callback(error, doc);
    });
};

exports.createRus = function(quote, callback) {
  db.get()
    .collection("quotes/rus")
    .insert(quote, function(error, doc) {
      callback(error, doc);
    });
};

exports.createEng = function(quote, callback) {
  db.get()
    .collection("quotes/eng")
    .insert(quote, function(error, doc) {
      callback(error, doc);
    });
};

exports.updateRus = function(id, author, quote, callback) {
  db.get()
    .collection("quotes/rus")
    .update(
      {
        _id: ObjectID(id)
      },
      {
        $set: {
          author: author,
          quote: quote
        }
      },
      {
        upsert: false,
        multi: false
      },
      function(error, doc) {
        callback(error, doc);
      }
    );
};

exports.updateEng = function(id, author, quote, callback) {
  db.get()
    .collection("quotes/eng")
    .update(
      {
        _id: ObjectID(id)
      },
      {
        $set: {
          author: author,
          quote: quote
        }
      },
      {
        upsert: false,
        multi: false
      },
      function(error, doc) {
        callback(error, doc);
      }
    );
};

exports.deleteRus = function(id, callback) {
  db.get()
    .collection("quotes/rus")
    .deleteOne(
      {
        _id: ObjectID(id)
      },
      function(error, result) {
        callback(error, result);
      }
    );
};

exports.deleteEng = function(id, callback) {
  db.get()
    .collection("quotes/eng")
    .deleteOne(
      {
        _id: ObjectID(id)
      },
      function(error, result) {
        callback(error, result);
      }
    );
};
