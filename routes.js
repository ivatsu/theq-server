module.exports = Object.freeze({
  ROOT: "/quotes",
  RUS: "/quotes/rus",
  ENG: "/quotes/eng",
  RUS_ID: "/quotes/rus/:id",
  ENG_ID: "/quotes/eng/:id"
});
