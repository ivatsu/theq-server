const express = require("express");
const bodyParser = require("body-parser");

const db = require("./config/db");
const routes = require("./routes");
const quotesController = require("./controllers/qoutes");

const app = express();
const dbUrl = "mongodb://localhost:27017";

app.use(bodyParser.json()); // to parse .json
app.use(bodyParser.urlencoded({ extended: true })); // to parse form data

app.get(routes.ROOT, function(req, res) {
  res.send(
    "Hello here is a quotes API. To access Russian quotes, use /rus or /eng to access English ones"
  );
});

app.get(routes.RUS, quotesController.allRus);
app.get(routes.ENG, quotesController.allEng);

// add new RUS quote
app.post(routes.RUS, quotesController.createRus);
// add new ENG quote
app.post(routes.ENG, quotesController.createEng);
// :id is a dynamic argument for id
app.get(routes.RUS_ID, quotesController.findRusById);
// :id is a dynamic argument for id
app.get(routes.ENG_ID, quotesController.findEngById);
// update RUS quote
app.put(routes.RUS_ID, quotesController.updateRus);
// update ENG quote
app.put(routes.ENG_ID, quotesController.updateEng);
// delete RUS quote
app.delete(routes.RUS_ID, quotesController.deleteRus);
// delete ENG quote
app.delete(routes.ENG_ID, quotesController.deleteEng);

db.connect(
  dbUrl,
  function(error) {
    if (error) {
      console.log(error);
    }

    app.listen(3010, function() {
      console.log("Server started at port 3010");
    });
  }
);
