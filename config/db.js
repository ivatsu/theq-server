const MongoClient = require("mongodb").MongoClient;
const dbName = "quotesDatabase";

const state = {
  db: null
};

exports.connect = function(url, done) {
  if (state.db) {
    return done();
  }

  MongoClient.connect(
    url,
    function(error, db) {
      if (error) {
        return done(error);
      }

      state.db = db.db(dbName);
      done();
    }
  );
};

exports.get = function() {
  return state.db;
};
