const Quotes = require("./../models/quotes");

exports.allRus = function(req, res) {
  Quotes.allRus(function(error, docs) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.send(docs);
  });
};

exports.allEng = function(req, res) {
  Quotes.allEng(function(error, docs) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.send(docs);
  });
};

exports.findRusById = function(req, res) {
  Quotes.findRusById(req.params.id, function(error, doc) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.send(doc);
  });
};

exports.findEngById = function(req, res) {
  Quotes.findEngById(req.params.id, function(error, doc) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.send(doc);
  });
};

exports.createRus = function(req, res) {
  const quote = {
    author: req.body.author,
    quote: req.body.quote,
    category: req.body.category
  };

  Quotes.createRus(quote, function(error, doc) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.send(quote);
  });
};

exports.createEng = function(req, res) {
  const quote = {
    author: req.body.author,
    quote: req.body.quote,
    category: req.body.category
  };

  Quotes.createEng(quote, function(error, doc) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.send(quote);
  });
};

exports.updateRus = function(req, res) {
  const quote = {
    author: req.body.author,
    quoteContent: req.body.quote,
    category: req.body.category
  };

  Quotes.updateRus(
    req.params.id,
    quote.author,
    quote.quoteContent,
    quote.category,
    function(error, doc) {
      if (error) {
        console.log(error);
        return res.sendStatus(500);
      }

      res.sendStatus(200);
    }
  );
};

exports.updateEng = function(req, res) {
  const quote = {
    author: req.body.author,
    quoteContent: req.body.quote,
    category: req.body.category
  };

  Quotes.updateEng(
    req.params.id,
    quote.author,
    quote.quoteContent,
    quote.category,
    function(error, doc) {
      if (error) {
        console.log(error);
        return res.sendStatus(500);
      }

      res.sendStatus(200);
    }
  );
};

exports.deleteRus = function(req, res) {
  Quotes.deleteRus(req.params.id, function(error, doc) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.sendStatus(200);
  });
};

exports.deleteEng = function(req, res) {
  Quotes.deleteEng(req.params.id, function(error, doc) {
    if (error) {
      console.log(error);
      return res.sendStatus(500);
    }

    res.sendStatus(200);
  });
};
